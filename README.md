# JUPYTER DOCKER SERVICE

## OVERVIEW

This is docker service that runs jupyterhub and then spawns jupyter notebooks
for individual users.

The code here is a modification of code from the
[Neils Bohr Institute](https://github.com/rasmunk/jupyter_service).


## HOW IT WORKS

The service is comprised of a jupyterhub layer that handles user authentication
and the starting of jupyter notebooks each user.

Authentication can be handled by either CAS (Central Authentication Service) or
AD (Active Directory), where data access rules are handled by the AD system in
both cases.


## INSTALLATION

Git the code

```bash
git clone git@gitlab.maxiv.lu.se:scisw/jupyterhub/jupyter-docker-service.git
```

Configure the location of the ssl certificates (won't be able to run without
them), port and host name:


### DOCKER CONFIGURATION

There is an example configuration file which should first be copied to the
proper file path:
```bash
cp maxiv/docker-compose.yml.example maxiv/docker-compose.yml
```

And then edited as needed, probably in three places:

1. SSL certificates
```bash
volumes:
- /etc/ssl/localcerts/w-v-hdf5view-0_maxiv_lu_se.crt:/etc/jupyterhub/ssl_cert.crt:ro
- /etc/ssl/localcerts/w-v-hdf5view-0_maxiv_lu_se.key:/etc/jupyterhub/ssl_cert.key:ro
```

2. Hostname
```bash
hostname: hdf5view.maxiv.lu.se
```

3. JupyterHub image
```bash
image: maxiv-scisw-jupyter/jupyterhub:local
```

### JUPYTERHUB CONFIGURATION

There is an example configuration file which should first be copied to the
proper file path:
```bash
cp maxiv/jupyterhub_config.py.example maxiv/jupyterhub_config.py
```

Then configure the host name:
```bash
vim maxiv/jupyterhub_config.py

c.JupyterHub.hub_ip = 'hdf5view.maxiv.lu.se'
```

#### AUTHENTICATION
User authentication is setup currently to use either CAS or AD.

The type of authentication should be selected:
```bash
cas_auth.JupyterHub.authenticator_class = 'ldapauthenticator.LDAPAuthenticator'

c.JupyterHub.authenticator_class = \
    'jhub_cas_authenticator.cas_auth.CASLocalAuthenticator'
```

Then, even if using CAS, configure the AD, as the code will lookup user and
group information in the AD that is not provided by CAS.

```bash
c.LDAPAuthenticator.server_address = 'ldaps://MYSERVER.LU.SE'
c.LDAPAuthenticator.server_port = 636
c.LDAPAuthenticator.use_ssl = True
c.LDAPAuthenticator.lookup_dn_search_user = 'MYUSER@MYLAB.LU.SE'
c.LDAPAuthenticator.lookup_dn_search_password = 'SECRETPASSWORD'
c.LDAPAuthenticator.bind_dn_template = [
    "cn={username},CN=Users,DC=maxlab,DC=lu,DC=se",
    "cn={username},OU=DUOactive,DC=maxlab,DC=lu,DC=se",
    "cn={username},OU=DUO,DC=maxlab,DC=lu,DC=se",
]
c.LDAPAuthenticator.lookup_dn = True
c.LDAPAuthenticator.lookup_dn_search_filter = '({login_attr}={login})'
c.LDAPAuthenticator.user_search_base = 'dc=maxlab,dc=lu,dc=se'
c.LDAPAuthenticator.user_attribute = 'sAMAccountName'
c.LDAPAuthenticator.lookup_dn_user_dn_attribute = 'cn'
c.LDAPAuthenticator.escape_userdn = False
```

And if using CAS, configure the CAS stuff, which is a bit simpler:
```bash
c.CASLocalAuthenticator.cas_login_url = 'https://CAS.MYLAB.LU.SE/cas/login'
c.CASLocalAuthenticator.cas_logout_url = 'https://CAS.MYLAB.LU.SE/cas/logout'
c.CASLocalAuthenticator.cas_service_validate_url = \
    'https://CAS.MYLAB.LU.S/cas/p3/serviceValidate'
c.CASLocalAuthenticator.cas_service_url = \
    'https://hdf5view.maxiv.lu.se:443/hub/login'
c.CASLocalAuthenticator.create_system_users = True
```


#### MOUNTED DIRECTORIES
Mount the necessary directories, which will be some collection of data and
home directories.  For example, the present setup of MAXIV requires these
lines to be added to the /etc/fstab file:

```bash
sudo vim /etc/fstab

  stor-0.maxiv.lu.se:/mxn/home /mxn/home nfs defaults,nosuid,_netdev,nfsvers=3 0 0
  stor-0.maxiv.lu.se:/mxn/visitors /mxn/visitors nfs noauto,defaults,nosuid,_netdev,nfsvers=3 0 0
  stor-1.maxiv.lu.se:/mxn/groups /mxn/groups nfs defaults,nosuid,_netdev,nfsvers=3 0 0
  offline-2.maxiv.lu.se:/gpfs/offline1/visitors /nfs/offline1/visitors nfs defaults,nfsvers=3,proto=tcp,nosuid,_netdev 0 0
  offline-2.maxiv.lu.se:/gpfs/offline1/staff /nfs/offline1/staff nfs defaults,nfsvers=3,proto=tcp,nosuid,_netdev 0 0
```

Then make directories if they do not already exist:

```bash
sudo mkdir -p /mxn/groups/
sudo mkdir -p /mxn/home/
sudo mkdir -p /mxn/visitors/
sudo mkdir -p /nfs/offline1/staff
sudo mkdir -p /nfs/offline1/visitors
```

And mount them:

```bash
sudo mount /mxn/groups/
sudo mount /mxn/home/
sudo mount /mxn/visitors/
sudo mount /nfs/offline1/staff
sudo mount /nfs/offline1/visitors
```

Then configure jupyterhub to use these directories, and specify the location of
the working directory, into which jupyter notebooks will be saved. Note that
{username} will be replaced with the logged in user's MAX IV user name, e.g.
jasbru, and {home_dir_path} will be replaced by mxn/home for staff members, and
mxn/visitors for external users when the the jupyter notebook image is spawned:

```bash
vim maxiv/jupyterhub_config.py

  c.SwarmSpawner.notebook_dir = '/{home_dir_path}/jupyter_notebooks/'

  mounts = [
      {'type': 'bind',
       'source': '/data/',
       'target': '/data/',
       'read_only': False},
      {'type': 'bind',
       'source': '{home_dir_path}/',
       'target': '{home_dir_path}/',
       'read_only': False},
      {'type': 'bind',
       'source': '/mxn/groups/',
       'target': '/mxn/groups/',
       'read_only': False},
  ]
```


#### JUPYTERLAB DOCKER IMAGES
Configure the jupyter notebook images that should be available:
```bash
vim maxiv/jupyterhub_config.py

  c.SwarmSpawner.dockerimages = [
      {'image': 'maxiv/hdf5-notebook:latest',
       'name': 'MAXIV HDF5 Notebook'},
      {'image': 'maxiv/base-notebook:latest',
       'name': 'MAXIV Base Notebook'},
  ]
```

Note that it's best to leave the image names WITHOUT the docker registry
prepended!
The reason for this is that otherwise jupyterhub may attempt to dowload the
image each time it is selected by a user, which is very slow and will result
in a timeout.
There are scripts that will be exectued to download newer versions of the
images when they are available, and then tag them for loacl use.


#### RESOURCE LIMITS
Limits on computing resources can also be set for each spawned notebook image:
```bash
vim maxiv/jupyterhub_config.py

  c.SwarmSpawner.resource_spec = {
      'cpu_limit': int(4 * 1e9),
      'mem_limit': int(4 * (1024**3))
  }
```


## RUNNING
A makefile has been created to simplify the control of the service:

![makefile screenshot](screenshots/make_output.png)

```bash
make start
```

## MONITORING
```bash
make logs
make monitor
```

## USING
```bash
https://jupyterhub.maxiv.lu.se/
```

![menu screenshot](screenshots/menu_page.png)

## STOPPING
```bash
make stop
```
