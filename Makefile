###############################################################################
#
# 	Jupyter Hub Docker Service - Makefile
#
#   This Makefile is used for starting and stopping the JupyterHub Service.
#
#  	Run 'make help' for usage information
#
###############################################################################

.DEFAULT_GOAL := help
SHELL := /bin/bash
.ONESHELL:
export PYTHONPATH=.

# Colors
LIGHTPURPLE := \033[1;35m
GREEN := \033[32m
CYAN := \033[36m
BLUE := \033[34m
RED := \033[31m
NC := \033[0m

# Seperator lines
line_len=80
print_line_green = printf "$(GREEN)"; printf "%0.s*" {1..$(line_len)}; \
				   printf "$(NC)\n"
print_line_blue = printf "$(BLUE)"; printf "%0.s*" {1..$(line_len)}; \
				  printf "$(NC)\n"
print_line_red = printf "$(RED)"; printf "%0.s*" {1..$(line_len)}; \
				 printf "$(NC)\n"

THIS_FILE := $(lastword $(MAKEFILE_LIST))

###############################################################################
##@ Help
###############################################################################

.PHONY: help

help:  ## Display this help message
	@printf "\n"
	$(print_line_blue)
	printf "$(RED)    Jupyter Docker Service $(BLUE)Makefile$(NC)\n"
	printf "    This Makefile is used for starting and\n"
	printf "    stopping the JupyterHub Service.\n"
	printf "\n$(BLUE)Usage\n    $(CYAN)make $(NC)<target>\n"
	printf "\n$(BLUE)Examples$(NC)\n"
	printf "    $(CYAN)make $(NC)start$(NC)\n"
	printf "    $(CYAN)make $(NC)logs$(NC)\n"
	printf "    $(CYAN)make $(NC)monitor$(NC)\n"
	printf "    $(CYAN)make $(NC)stop$(NC)\n"
	awk 'BEGIN {FS = ":.*##";} /^[a-zA-Z_-].*##/ \
	{ printf "    $(CYAN)%-15s$(NC) %s\n", $$1, $$2} /^##@/ \
	{ printf "\n$(BLUE)%s$(NC)\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
	$(print_line_blue)


###############################################################################
##@ Service Control
###############################################################################

.PHONY: start stop restart logs monitor

start: ## Start the service
	@$(print_line_green)
	printf "$(GREEN)**$(CYAN) Starting the Jupyter Hub Service $(NC)\n"
	env DEV=0 DEBUG="" \
		HTTPS_PORT=443 HTTP_PORT=80 \
		docker stack deploy \
		--compose-file maxiv/docker-compose.yml jupyter-service-live
	printf "$(GREEN)**$(CYAN) Done Starting the Jupyter Hub Service $(NC)\n"
	$(print_line_green)

stop: ## Stop the service
	@$(print_line_red)
	printf "$(RED)**$(BLUE) Stopping the Jupyter Hub Service $(NC)\n"
	docker service ls --filter name=jupyter-service-live -q | xargs docker service rm && \
		docker stack rm jupyter-service-live && \
		sleep 10
	printf "$(RED)**$(BLUE) Pruning the Docker System $(NC)\n"
	docker system prune -f
	printf "$(RED)**$(BLUE) Removing volume$(NC)\n" && \
	docker volume rm jupyter-service-live_jhubstate
	docker volume prune -f
	printf "$(RED)**$(BLUE) Done Stopping the Jupyter Hub Service $(NC)\n"
	$(print_line_red)

restart: stop start ## Restart the service

logs: ## View service logs while running
	@$(print_line_green)
	printf "$(GREEN)**$(CYAN) Viewing docker logs for the Jupyter Hub"
	printf " Service $(NC)\n"
	printf "$(GREEN)**$(CYAN) (ctrl-c to exit) $(NC)\n"
	docker service logs jupyter-service-live_jupyterhub -ft

monitor: ## Monitor the notebook containers
	@$(print_line_green)
	printf "$(GREEN)**$(CYAN) Starting docker monitor of all containers$(NC)\n"
	docker run -it -v /var/run/docker.sock:/var/run/docker.sock moncho/dry
	printf "$(GREEN)**$(CYAN) Closed docker monitor of containers$(NC)\n"
	$(print_line_green)


###############################################################################
##@ Development
###############################################################################

.PHONY: start-dev stop-dev logs-dev

start-dev: ## Start the development version of the service
	@$(print_line_green)
	printf "$(GREEN)**$(CYAN) Starting the Jupyter Hub Service $(NC)\n"
	env DEV=1 DEBUG="--debug" \
		HTTPS_PORT=7443 HTTP_PORT=7780 \
		docker stack deploy \
		--compose-file maxiv/docker-compose.yml jupyter-service-dev
	printf "$(GREEN)**$(CYAN) Done Starting the Jupyter Hub Service $(NC)\n"
	$(print_line_green)

stop-dev: ## Stop the development version of the service
	@$(print_line_red)
	printf "$(RED)**$(BLUE) Stopping the Jupyter Hub Service $(NC)\n"
	docker service ls --filter name=jupyter-service-dev -q | xargs docker service rm && \
		docker stack rm jupyter-service-dev && \
		sleep 10
	printf "$(RED)**$(BLUE) Pruning the Docker System $(NC)\n"
	docker system prune -f
	printf "$(RED)**$(BLUE) Removing volume$(NC)\n" && \
	docker volume rm jupyter-service-dev_jhubstate
	docker volume prune -f
	printf "$(RED)**$(BLUE) Done Stopping the Jupyter Hub Service $(NC)\n"
	$(print_line_red)

logs-dev: ## View service logs while running
	@$(print_line_green)
	printf "$(GREEN)**$(CYAN) Viewing docker logs for the Jupyter Hub"
	printf " Service $(NC)\n"
	printf "$(GREEN)**$(CYAN) (ctrl-c to exit) $(NC)\n"
	docker service logs jupyter-service-dev_jupyterhub -ft

